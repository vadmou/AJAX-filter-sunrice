<?php
require_once('../../components/Db.php');
$db = Db::getConnection();
if (isset($_POST)) {

    if ($_POST["flag"] == "1") {
        $city_id = $_POST['city'];
        $type_id = $_POST['type'];

        if ((int)$type_id == 1) {
            $col_type = "a.price";
            if (isset($_POST['from_id']) && $_POST['from_id'] != "") {
                $from_result = $_POST['from_id'] - (($_POST['from_id']) / 100) * 10;
            } else {
                $from_result = '0';
            }

            if (isset($_POST['to_id']) && $_POST['to_id'] != "") {
                $to_result = $_POST['to_id'] + (($_POST['to_id']) / 100) * 10;
            } else {
                $to_result = '999999';
            }

        } else if ((int)$type_id == 2) {
            $col_type = "a.area";
            if (isset($_POST['from_id']) && $_POST['from_id'] != "") {
                $from_result = $_POST['from_id'] - (($_POST['from_id']) / 100) * 10;
            } else {
                $from_result = '0';
            }

            if (isset($_POST['to_id']) && $_POST['to_id'] != "") {
                $to_result = $_POST['to_id'] + (($_POST['to_id']) / 100) * 10;
            } else {
                $to_result = '999999';
            }
        }

        $Apartments = array();

        $result = $db->query('
            SELECT a.area, a.price, c.city_name
			  FROM city c
			  RIGHT JOIN apartments a ON c.id=a.city_id
			  WHERE a.city_id ' . $city_id . ' AND a.type_id = ' . $type_id . '
            AND ' . $col_type . ' BETWEEN ' . $from_result . ' AND ' . $to_result);

        $i = 0;
        while ($row = $result->fetch()) {

            $Apartments[$i]['city_name'] = $row['city_name'];
            $Apartments[$i]['area'] = $row['area'];
            $Apartments[$i]['price'] = $row['price'];
            $i++;
        }
        print(json_encode($Apartments));
    }

    if ($_POST["flag"] == "2") {
        $city_id = $_POST['city'];
        $type_id = $_POST['type'];

        if ((int)$type_id == 1) {
            $col_type = "a.price";
            if (isset($_POST['from_id']) && $_POST['from_id'] != "") {
                $from_result = $_POST['from_id'] - (($_POST['from_id']) / 100) * 10;
            } else {
                $from_result = '0';
            }

            if (isset($_POST['to_id']) && $_POST['to_id'] != "") {
                $to_result = $_POST['to_id'] + (($_POST['to_id']) / 100) * 10;
            } else {
                $to_result = '999999';
            }

        } else if ((int)$type_id == 2) {
            $col_type = "a.area";
            if (isset($_POST['from_id']) && $_POST['from_id'] != "") {
                $from_result = $_POST['from_id'] - (($_POST['from_id']) / 100) * 10;
            } else {
                $from_result = '0';
            }

            if (isset($_POST['to_id']) && $_POST['to_id'] != "") {
                $to_result = $_POST['to_id'] + (($_POST['to_id']) / 100) * 10;
            } else {
                $to_result = '999999';
            }
        }

        $Apartments = array();

        $result = $db->query('
            SELECT a.area, a.price, c.city_name
			  FROM city c
			  RIGHT JOIN apartments a ON c.id=a.city_id
			  WHERE a.city_id ' . $city_id . '
            AND ' . $col_type . ' BETWEEN ' . $from_result . ' AND ' . $to_result);

        $i = 0;
        while ($row = $result->fetch()) {

            $Apartments[$i]['city_name'] = $row['city_name'];
            $Apartments[$i]['area'] = $row['area'];
            $Apartments[$i]['price'] = $row['price'];
            $i++;
        }
        print(json_encode($Apartments));
    }
}

?>