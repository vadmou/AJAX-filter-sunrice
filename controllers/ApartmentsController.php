<?php

include_once ROOT . '/models/Apartments.php';

class ApartmentsController
{

    public function actionIndex()
    {

        $cityList = array();
        $cityList = Apartments::getCity();

        require_once(ROOT . '/views/index.php');

        return true;
    }

    public function actionElse()
    {

        $cityList = array();
        $cityList = Apartments::getCity();

        require_once(ROOT . '/views/else.php');

        return true;
    }

}
