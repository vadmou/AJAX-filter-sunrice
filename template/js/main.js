window.onload = function () {
    var $type = $("input[name = type_id]").val();

    $("input[name = type_id]").change(function () {
        //alert($(this).val());
        if ($(this).val() == 1) {
            $("#head").empty().append("Цена");
        } else if ($(this).val() == 2) {
            $("#head").empty().append("Площадь");
        }
        $type = $(this).val();
    });
    $("input[name = submit]").click(function () {
        var $from_id = $("input[name = from]").val();
        var $to_id = $("input[name = to]").val();
        var $city = $("select[name = city_id]").val();
        $.ajax({
            url: "/template/ajax/main.php",
            method: 'post',
            dataType: 'json',
            data: {
                type: $type,
                from_id: $from_id,
                to_id: $to_id,
                city: $city,
                flag: "1"
            },
            success: function (data) {
                var str = "";
                for (var id in data) {
                    str += "<tr><td>" + data[id]["city_name"] + "</td>\
					<td>" + data[id]["price"] + "</td>\
					<td>" + data[id]["area"] + "</td></tr>";
                }
                $("#filtred tbody").empty().append(str);
            },
            error: function () {
                console.log("error");
            }
        })
    });
}