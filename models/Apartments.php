<?php

class Apartments
{

    public static function getCity()
    {
        $db = Db::getConnection();

        $cityList = array();

        $result = $db->query('SELECT id, city_name FROM city');

        $i = 0;
        while ($row = $result->fetch()) {
            $cityList[$i]['id'] = $row['id'];
            $cityList[$i]['city_name'] = $row['city_name'];
            $i++;
        }

        return $cityList;
    }

}