<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Выбор квартиры</title>
    <link rel="stylesheet" type="text/css" href="/template/css/style.css">
    <script src="/template/js/jquery-3.1.1.min.js"></script>
    <script src="/template/js/main.js"></script>
</head>

<body>
<a href="/">База данных разделена на "Ареднда" и "Продажа"</a><br>
<a href="/else">База данных "общая"</a><br>
<br>
<div class="layout">
    <div class="sidebar">

        <form action="#" method="post" name="Form" onsubmit="return false;">

            <input type="radio" name="type_id" value="1" checked>Продажа
            <input type="radio" name="type_id" value="2">Аренда<br>
            <br>
            <div>
                <select name="city_id">
                    <?php foreach ($cityList as $city): ?>
                        <option value="= <?php echo $city['id']; ?>"><?php echo $city['city_name']; ?></option>
                    <?php endforeach; ?>
                </select>
            </div>
            <br>
            <div class="item">

                <div id="price">
                    <h4 id="head">Цена:</h4>
                    <input type="text" name="from" placeholder="от">
                    <input type="text" name="to" placeholder="до">
                    <br>
                </div>
            </div>
            <br>

    </div>
    <br>
    <input type="submit" value="Применить фильтр" name="submit">
    </form>
    <br>
</div>
<div class="content"><h3>Результаты поиска</h3>
    <table border="1px solid black" id="filtred">
        <thead>
        <tr class="column-name">
            <td>Город</td>
            <td>Цена</td>
            <td>Площадь</td>
        </tr>
        </thead>
        <tbody>

        </tbody>
    </table>
</div>
</div>
</body>
</html>